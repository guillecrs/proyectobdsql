************************************
*************INTEGRANTES************
** CUEVAS MADRIGAL ALEJANDRO      **
** CRUZ SALAZAR GUILLERMO ARMANDO **
************************************
************************************
1.¿Qué  actores  tienen  el  primer  nombre  'Scarlett'?
2.¿Qué  actores  tienen  el  apellido  'Johansson'?
3.¿Cuántos  apellidos  de  actores  distintos  hay?
4.¿Qué  apellidos  no  se  repiten?
5.¿Qué  apellidos  aparecen  más  de  una  vez?
6.¿Qué  actor  ha  aparecido  en  la  mayoría  de  las  películas?
7.¿Se  puede  alquilar  'Academy  Dinosaur'  desde  la  tienda  1?
8.Inserte  un  registro  para  representar  a  Mary  Smith  al  alquilar  'Dinosaurio  de  la  Academia'  de  Mike  Hillyer  en  la  Tienda  1  hoy.
9.¿Cuándo  se  debe  'Academia  Dinosaurio'?
10.¿Cuál  es  el  tiempo  promedio  de  ejecución  de  todas  las  películas  en  el  DB  de  
sakila?
11.¿Cuál  es  el  tiempo  promedio  de  duración  de  las  películas  por  categoría?
12.¿Cuál es  el actor  que  ha  actuado  en  más  películas?
13.Consulta  que  regrese  todas  las  direcciones  sin  teléfonos
14.¿Cuál es  la  película  más  antigua?

15.¿Cual  es  la  existencia  de  cada  película en  todas  las  tienes?

a.Responder  con  un  reporte  con  la  siguiente estructura

Nombre Tienda            Nombre Película   Existencia
Culiacán                 XXXXXXXXXXXXXXXX     10
MORELIA                         YYYYYYY        5
16.Modifica  el  query  del  punto  anterior  para  que  incluya  
también las  películas que  no  tienen  registros  en  la  tabla  inventory

17.¿Cuál es el  numero  de  clientes  por  ciudad ?

18.Cual  es  la  tienda  con  mas  empleados

19.Inserta  al  menos  5  películas en  las  categoría  “Mexicanas”,  “Frikis”
a.Inserta  category
b.Inserta  actor
c.Inserta  en  film
d.Insertaen  film_actor
e.Inserta  en  film_category

20.Inserta  una  tienda  nueva  en  “Culiacan”
a.Insertar  registro  para  Country
b.Insertar  registro  para  City
c.Insertar  Store  Culiacán
d.Insertar  manager,  empleado  en  staff
